# Trongle SS13
Trongle SS13 is a roleplaying game built on the BYOND game engine. It is based off of the [/tg/station codebase](https://github.com/tgstation/tgstation). Players take control of a character on a space station in the mid 26th century and must deal with threats varying from meteor strikes to rogue wizards, all while working to keep the space station running. This repository holds the source code and resources necessary for compiling a complete server binary.

* **Website:** https://trongle.net
* **Code:** https://gitlab.com/trongle/trongle-ss13

## DOWNLOADING
Option 1 (RECOMMENDED): Clone this repo using git

Option 2: Download a zip of the source code here: https://gitlab.com/trongle/trongle-ss13/-/archive/master/trongle-ss13-master.zip  
(note: only choose this option if you aren't planning on contributing)

## USAGE
### Note for non-Windows users
This codebase depends on a native library called rust-g. A precompiled Windows DLL is included in this repository, but Linux users will need to build and install it themselves. Directions can be found at the rust-g repo: https://github.com/tgstation/rust-g

### Instructions
You'll need BYOND installed. You can get it from https://www.byond.com/download. 

1. Open the downloaded source code
2. Double-click ss13.dme to open it in the Dreammaker IDE, open the Build menu, and click compile. This will take around 1-5 minutes and will produce a compiled ss13.dmb file.
    * If compiling produced any errors, contact a developer for help.
3. Edit config/admins.txt to remove the default admins and add your own. You can set up your own ranks and find out more in config/admin_ranks.txt
4. Edit config/config.txt to change the port that you will use as well as other settings such as server name, host name, etc.
5. Open the Dream Daemon program and enter the path to your compiled ss13.dmb file. Set the port to the one you specified in the config.txt. Set the Security box to 'Safe'. Then press GO

It is also recommended that you set up a database.

### Database setup
The SQL backend requires a Mariadb server running 10.2 or later. Mysql is not supported but Mariadb is a drop in replacement for mysql. SQL is required for the library, stats tracking, admin notes, and job-only bans, among other features, mostly related to server administration. Your server details go in /config/dbconfig.txt, and the SQL schema is in /SQL/db_schema.sql and /SQL/db_schema_prefix.sql depending on if you want table prefixes.  More detailed setup instructions are located on the /tg/ wiki here: https://www.tgstation13.org/wiki/Downloading_the_source_code#Setting_up_the_database

If you are hosting a testing server on windows you can use a standalone version of MariaDB preloaded with a blank (but initialized) tgdb database. Find them here: https://tgstation13.download/database/ Just unzip and run for a working (but insecure) database server. Includes a zipped copy of the data folder for easy resetting back to square one.

### Updating
To update an existing installation, first back up your /config and /data folders as these store your server configuration, player preferences and banlist.

Then, extract the new files (preferably into a clean directory, but updating in place should work fine), copy your /config and /data folders back into the new install, overwriting when prompted except if we've specified otherwise, and recompile the game.  Once you start the server up again, you should be running the new version.

### Web/CDN resource delivery
Web delivery of game resources makes it quicker for players to join and reduces some of the stress on the game server.

1. Edit compile_options.dm to set the `PRELOAD_RSC` define to `0`
1. Add a url to config/external_rsc_urls pointing to a .zip file containing the .rsc.
    * You can use cdn services like CDN77 or cloudflare (requires adding a page rule to enable caching of the zip), or roll your own cdn using route 53 and vps providers.
	* Even offloading the rsc to a website without a CDN will be a massive improvement over the in game system for transferring files.

### IRC bot setup
Included in the repository is a python3 compatible IRC bot capable of relaying adminhelps to a specified IRC channel/server, see the /tools/minibot folder for more

## MAPS
Trongle SS13 currently comes equipped with five maps.

* BoxStation (default): https://tgstation13.org/wiki/Boxstation
* MetaStation: https://tgstation13.org/wiki/MetaStation
* DeltaStation: https://tgstation13.org/wiki/DeltaStation
* PubbyStation: https://tgstation13.org/wiki/PubbyStation
* DonutStation: https://tgstation13.org/wiki/Donutstation

All maps have their own code file that is in the base of the _maps directory. Maps are loaded dynamically when the game starts. Follow this guideline when adding your own map, to your fork, for easy compatibility.

The map that will be loaded for the upcoming round is determined by reading data/next_map.json, which is a copy of the json files found in the _maps tree. If this file does not exist, the default map from config/maps.txt will be loaded. Failing that, BoxStation will be loaded. If you want to set a specific map to load next round you can use the Change Map verb in game before restarting the server or copy a json from _maps to data/next_map.json before starting the server. Also, for debugging purposes, ticking a corresponding map's code file in Dream Maker will force that map to load every round.

If you are hosting a server, and want randomly picked maps to be played each round, you can enable map rotation in [config.txt](config/config.txt) and then set the maps to be picked in the [maps.txt](config/maps.txt) file.

Anytime you want to make changes to a map it's imperative you use the Map Merging tools: https://tgstation13.org/wiki/Map_Merger

### Away Missions
Trongle SS13 supports loading away missions however they are disabled by default.

Map files for away missions are located in the _maps/RandomZLevels directory. Each away mission includes it's own code definitions located in /code/modules/awaymissions/mission_code. These files must be included and compiled with the server beforehand otherwise the server will crash upon trying to load away missions that lack their code.

To enable an away mission open `config/awaymissionconfig.txt` and uncomment one of the .dmm lines by removing the #. If more than one away mission is uncommented then the away mission loader will randomly select one the enabled ones to load.

## CONTRIBUTING
Please see [our contributing guidelines](.gitlab/contributing.md)

## LICENSE
All code after [commit 333c566b88108de218d882840e61928a9b759d8f on 2014/31/12 at 4:38 PM PST](https://gitlab.com/trongle/trongle-ss13/commit/333c566b88108de218d882840e61928a9b759d8f) is licensed under [GNU AGPL v3](https://www.gnu.org/licenses/agpl-3.0.html).

All code before [commit 333c566b88108de218d882840e61928a9b759d8f on 2014/31/12 at 4:38 PM PST](https://gitlab.com/trongle/trongle-ss13/commit/333c566b88108de218d882840e61928a9b759d8f) is licensed under [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).
(Including tools unless their readme specifies otherwise.)

See LICENSE and GPLv3.txt for more details.

tgui clientside is licensed as a subproject under the MIT license.
Font Awesome font files, used by tgui, are licensed under the SIL Open Font License v1.1
tgui assets are licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
The TGS3 API is licensed as a subproject under the MIT license.

See tgui/LICENSE.md for the MIT license.
See tgui/assets/fonts/SIL-OFL-1.1-LICENSE.md for the SIL Open Font License.
See the footers of code/\_\_DEFINES/server\_tools.dm, code/modules/server\_tools/st\_commands.dm, and code/modules/server\_tools/st\_inteface.dm for the MIT license.

All assets including icons and sound are under a [Creative Commons 3.0 BY-SA license](https://creativecommons.org/licenses/by-sa/3.0/) unless otherwise indicated.
