<!--
	Please check if your issue has already been reported before filling
	out a bug report. If a specific field doesn't apply, remove it!.

	Anything inside tags like these is a comment and will not be displayed.	
	Headers are preceded by ### 
-->
### Issue Description
<!--  A clear and concise description of what the issue is  -->

### Expected Behavior
<!--  A short description of what you expected to happen  -->

### Reproducibility
<!--  Mark whatever applies: [ ] -> [x]  -->
- [ ] Experienced more than once
- [ ] Experienced in multiple rounds
- [ ] Experienced by other players
- [ ] Occurred less than 7 days ago

### Steps To Reproduce
<!--  The steps to take to reproduce the issue  -->

### Screenshots
<!--  If applicable, add screenshots to help explain your issue  -->

### Additional
<!--  Add any other relevant information here  -->

### Client version, Server revision, Game ID
<!--  Found with the "Show server revision" verb in the OOC tab in game  -->


<!--  Don't touch anything below this line  -->
/milestone %"0.1.0"