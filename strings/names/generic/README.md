These files are used for generic randomized names. Each row contains a name and the number of people per 1,000,000 with that name.

first_male.txt and first_female.txt data comes from the [list of most popular baby names from 1990 provided by US Social Security](https://www.ssa.gov/cgi-bin/popularnames.cgi)
last.txt data comes from the [list of the most common last names in the US provided by the 2010 US Census](https://www.census.gov/topics/population/genealogy/data/2010_surnames.html)
