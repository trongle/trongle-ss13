# this script will take a list of strings, randomize the order, and then
# assign a weight to each entry 
# invoke this script like so
# `python gen_dist.py filename.txt 30000 10`

import random
import math
import sys

filename = sys.argv[1] # file of entries each on their own line
max_weight = float(sys.argv[2]) # starting occurence in our sample
min_weight = float(sys.argv[3]) # what we want 

if max_weight < min_weight:
    print("must provide a max_weight that is greater than or equal to min_weight")
    exit

if min_weight < 1.0:
    print("must provide a min_weight that is at least 1.0")
    exit

entries = list()
for entry in open(filename):
    entries.append(entry.strip())

entries = list(dict.fromkeys(entries)) # removing duplicate entries

entries_len = len(entries)
c = math.log(min_weight/max_weight, entries_len) # power function constant

output = open("out_"+filename, "w+")
for x in range(1, entries_len+1):
    entry = random.choice(entries)
    occurence = int(max_weight*(float(x)**c))
    if occurence == 0:
        occurence = 1
    output.write(entry + " " + str(occurence) + "\n")
