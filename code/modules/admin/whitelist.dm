#define WHITELISTFILE "[global.config.directory]/whitelist.txt"

GLOBAL_LIST(whitelist)
GLOBAL_PROTECT(whitelist)

/proc/load_whitelist()
	GLOB.whitelist = list()

	// failover to non-sql whitelist if database isn't configured
	if(!CONFIG_GET(flag/sql_enabled))
		log_config("Falling back to legacy whitelist system")
		for(var/line in world.file2list(WHITELISTFILE))
			if(!line)
				continue
			if(findtextEx(line,"#",1,2))
				continue
			GLOB.whitelist += ckey(line)
	else
		log_config("Loading whitelist from Database")
		var/datum/DBQuery/query_load_whitelist = SSdbcore.NewQuery("SELECT `ckey` FROM [format_table_name("whitelist")]")
		if(!query_load_whitelist.Execute())
			message_admins("Error loading whitelist from database.")
			log_sql("Error loading whitelist from database.")
		else
			while(query_load_whitelist.NextRow())
				GLOB.whitelist += query_load_whitelist.item[1]

		qdel(query_load_whitelist)

	if(!GLOB.whitelist.len)
		GLOB.whitelist = null

/client/proc/add_to_whitelist(mob/M in GLOB.mob_list - GLOB.dummy_mob_list)
	set category = "Admin"
	set name = "Add to Whitelist"
	set desc = "Add a user to the server whitelist"
	
	if(!src.holder)
		to_chat(src, "Only administrators may use this command.")
		return

	var/datum/DBQuery/insert_admin_whitelist = SSdbcore.NewQuery("INSERT INTO [format_table_name("whitelist")] (`ckey`, `a_ckey`) VALUES ('[usr.ckey]', '[key_name(M)]')")
	insert_admin_whitelist.Execute()
	qdel(insert_admin_whitelist)

	log_admin("[key_name(usr)] added [key_name(M)] to the whitelist")
	message_admins("[key_name_admin(usr)] added [ADMIN_LOOKUPFLW(M)] to the whitelist")

	SSblackbox.record_feedback("tally", "admin_verb", 1, "Add Whitelist") //If you are copy-pasting this, ensure the 2nd parameter is unique to the new proc!


/proc/check_whitelist(var/ckey, var/ip)
	if(GLOB.whitelist)
		if (ckey in GLOB.whitelist)
			return TRUE

	// Check if user IP address OR ckey has been added to the temporary table
	var/datum/DBQuery/select_ip_from_whitelist = SSdbcore.NewQuery("SELECT `code`,`ckey` FROM [format_table_name("whitelist_invites")] WHERE ( ips LIKE '%[ip]%' OR players LIKE '%:[ckey]:%' ) AND round_id IS NULL")
	select_ip_from_whitelist.Execute()
	if (select_ip_from_whitelist.NextRow())
		message_admins("[ckey] has logged in with code: [select_ip_from_whitelist.item[1]] from [select_ip_from_whitelist.item[2]]")
		qdel(select_ip_from_whitelist)

		// Add the ckey into the database
		var/datum/DBQuery/update_whitelist_ckey =  SSdbcore.NewQuery("UPDATE whitelist_invites SET players = CONCAT(':', '[ckey]', ':', players) WHERE ips LIKE '%[ip]%' AND ckey NOT LIKE '%:[ckey]:%'")
		update_whitelist_ckey.Execute()
		qdel(update_whitelist_ckey)

		return TRUE
	qdel(select_ip_from_whitelist)

	return FALSE

//This proc allows admins to reload the whitelist
/client/proc/reload_whitelist()
	set name = "Reload Whitelist"
	set desc = "Reloads the whitelist"
	set category = "Admin"

	load_whitelist()
	to_chat(src, "<span class='boldnotice'>Whitelist has been reloaded!</span>")

/client/verb/generate_whitelist_token()
	set name = "Invite Players"
	set desc = "Generate a token to let your friends temporarily play!"
	set category = "Special Verbs"

	// Only let people in the actual whitelist create these tokens
	if(GLOB.whitelist)
		if (!(ckey in GLOB.whitelist))
			to_chat(src, "<span class='warning'>This feature is only available for users already on the permanent whitelist! Be a good player and you'll soon enough get added!</span>")

	// Check if the player has already generated a token this round
	var/datum/DBQuery/query_whitelist_this_round = SSdbcore.NewQuery("SELECT code FROM [format_table_name("whitelist_invites")] WHERE ckey = '[src]' AND round_id IS NULL")
	query_whitelist_this_round.Execute()

	if(!query_whitelist_this_round.NextRow())
		var limit = 2
		// Give admins unlimited invites
		if(src.holder)
			limit = input("How many players is this key limited to? (default 2)", text("Input")) as num|null

		var/datum/DBQuery/insert_whitelist_invite = SSdbcore.NewQuery("INSERT INTO [format_table_name("whitelist_invites")] (`ckey`, `limit`, `code`) VALUES ('[src.ckey]', [limit], MD5(RAND()))")
		insert_whitelist_invite.Execute()
		qdel(insert_whitelist_invite)
		
		var/datum/DBQuery/whitelist_password = SSdbcore.NewQuery("SELECT code FROM [format_table_name("whitelist_invites")] WHERE id = LAST_INSERT_ID()")
		whitelist_password.Execute()

		if(whitelist_password.NextRow())
			to_chat(src, "<span class='boldnotice'>Temporary Whitelist Token with [limit] uses has been generated: [whitelist_password.item[1]]</span>")

		qdel(whitelist_password)
	else
		to_chat(src, "<span class='warning'>You have already created a temporary password this round!</span>")
		to_chat(src, "<span class='warning'>Whitelist password: [query_whitelist_this_round.item[1]]</span>")
		qdel(query_whitelist_this_round)
	
	qdel(query_whitelist_this_round)

#undef WHITELISTFILE
