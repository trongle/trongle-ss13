/mob/living/regenerate_icons(var/update = FALSE)
	update_transform()
	update_shadow()
	return

/mob/living/get_x_offset()
	. = ..()
	if(src.buckled)
		. += src.buckled.get_x_offset()

		var/datum/component/riding/riding_datum = src.buckled.GetComponent(/datum/component/riding)
		if(riding_datum)
			. += riding_datum.rider_get_x_offset(src)
	
	return .

/mob/living/get_y_offset()
	. = ..()

	if(src.buckled)
		. += src.buckled.get_y_offset()

		var/datum/component/riding/riding_datum = src.buckled.GetComponent(/datum/component/riding)
		if(riding_datum)
			. += riding_datum.rider_get_y_offset(src)
	
	return .

/mob/living/add_atom_orientation(var/matrix/M)
	if(src.rotate_on_lying)
		return M.Turn(src.lying)
	else
		return ..(M)

/mob/living/proc/apply_overlay(cache_index)
	if((. = src.overlays_cache[cache_index]))
		add_overlay(.)

/mob/living/proc/remove_overlay(cache_index)
	var/I = src.overlays_cache[cache_index]
	if(I)
		cut_overlay(I)
		src.overlays_cache[cache_index] = null

/mob/living/proc/update_shadow()
	remove_overlay(src.shadow_layer)

	if(src.is_lying_down()) // dont display shadows if we're laying down
		return

	var/mutable_appearance/shadow = mutable_appearance('icons/effects/shadows.dmi', "mob_[src.mob_size]", MOB_SHADOW_LAYER)
	shadow.pixel_y = SHADOW_Y_SHIFT-src.get_y_offset() // putting it lower than our mob
	shadow.pixel_x = src.shadow_x_shift
	shadow.mouse_opacity = 0 // we dont want the shadow to be clickable

	src.overlays_cache[src.shadow_layer] = shadow

	apply_overlay(src.shadow_layer)
