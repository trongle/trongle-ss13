/datum/airlock_paint
	var/list/frame_paint = list()
	var/list/fill_paint = list()

/datum/airlock_paint/proc/get_paint_name(var/piece)
	var/list/paint_list
	if(piece == "frame")
		paint_list = frame_paint
	else if(piece == "fill")
		paint_list = fill_paint
	else
		return

	var/name = ""
	for(var/layer in paint_list)
		name += layer
		name += paint_list[layer]
	return name

/datum/airlock_paint/proc/get_paint(var/piece, var/paint_file, var/state, var/mutable_appearance/icon)
	var/list/paint_list
	if(piece == "frame")
		paint_list = frame_paint
	else if(piece == "fill")
		paint_list = fill_paint
	else
		return

	for(var/layer in paint_list)
		var/list/paint_color = color_matrix_add(color_hex2color_matrix(paint_list[layer]), color_matrix_lightness(0.1))

		var/mutable_appearance/paint_mult = mutable_appearance(paint_file, "[layer]_[state]")		
		paint_mult.color = paint_color
		icon.overlays += paint_mult
	return icon

/datum/airlock_paint/command
	frame_paint = list("full" = "#183666")
	fill_paint = list("full" = "#183666")

/datum/airlock_paint/medical
	frame_paint = list(
	"full" = "#cfcfcf",
	"stripe" = "#2c8243")
	fill_paint = list(
	"full" = "#cfcfcf",
	"stripe" = "#2c8243")

/datum/airlock_paint/virology
	frame_paint = list(
	"full" = "#2c8243",
	"stripe" = "#cfcfcf")
	fill_paint = list(
	"full" = "#2c8243",
	"stripe" = "#cfcfcf")

/datum/airlock_paint/security
	frame_paint = list("full" = "#7a0a0a")
	fill_paint = list("full" = "#7a0a0a")

/datum/airlock_paint/high_security
	frame_paint = list(
	"full" = "#183666",
	"stripe" = "#7a0a0a")
	fill_paint = list(
	"full" = "#183666",
	"stripe" = "#7a0a0a")

/datum/airlock_paint/engineering
	frame_paint = list("full" = "#dba91d")
	fill_paint = list("full" = "#dba91d")

/datum/airlock_paint/atmospherics
	frame_paint = list(
	"full" = "#dba91d",
	"stripe" = "#1ddbd2")
	fill_paint = list(
	"full" = "#dba91d",
	"stripe" = "#1ddbd2")

/datum/airlock_paint/maintenance
	frame_paint = list("full" = "#383838")
	fill_paint = list("full" = "#383838")

/datum/airlock_paint/science
	frame_paint = list(
	"full" = "#cfcfcf",
	"stripe" = "#542d7a")
	fill_paint = list(
	"full" = "#cfcfcf",	
	"stripe" = "#542d7a")

/datum/airlock_paint/external
	frame_paint = list("full" = "#a8260c")
	fill_paint = list("full" = "#a8260c")

/datum/airlock_paint/external_maint
	frame_paint = list(
	"full" = "#383838",
	"stripe" = "#a8260c")
	fill_paint = list(
	"full" = "#383838",
	"stripe" = "#a8260c")

/datum/airlock_paint/supply
	frame_paint = list("full" = "#8f4f11")
	fill_paint = list("full" = "#8f4f11")

/datum/airlock_paint/freezer
	frame_paint = list("full" = "#cfcfcf")
	fill_paint = list("full" = "#cfcfcf")

/datum/airlock_paint/public
	frame_paint = list("full" = "#969696")
	fill_paint = list("full" = "#969696")

/datum/airlock_paint/centcomm
	frame_paint = list("full" = "#001c4c")
	fill_paint = list("full" = "#001c4c")

/datum/airlock_paint/vault
	frame_paint = list("full" = "#262337")
	fill_paint = list("full" = "#262337")

/datum/airlock_paint/hatch
	frame_paint = list("stripe" = "#a8260c")
	fill_paint = list("stripe" = "#a8260c")

/datum/airlock_paint/gold
	frame_paint = list("full" = "#c2a00c")
	fill_paint = list("full" = "#c2a00c")

/datum/airlock_paint/silver
	frame_paint = list("full" = "#c8cfcd")
	fill_paint = list("full" = "#c8cfcd")

/datum/airlock_paint/diamond
	frame_paint = list("full" = "#7ddbd7")
	fill_paint = list("full" = "#7ddbd7")

/datum/airlock_paint/uranium
	frame_paint = list("full" = "#51b85e")
	fill_paint = list("full" = "#51b85e")

/datum/airlock_paint/plasma
	frame_paint = list("full" = "#a751b8")
	fill_paint = list("full" = "#a751b8")

/datum/airlock_paint/bananium
	frame_paint = list("full" = "#e8dd46")
	fill_paint = list("full" = "#e8dd46")

/datum/airlock_paint/sandstone
	frame_paint = list("full" = "#bfa760")
	fill_paint = list("full" = "#bfa760")

/datum/airlock_paint/titanium
	frame_paint = list("full" = "#a7aeb0")
	fill_paint = list("full" = "#a7aeb0")

/datum/airlock_paint/bronze
	frame_paint = list("full" = "#db8f32")
	fill_paint = list("full" = "#db8f32")
