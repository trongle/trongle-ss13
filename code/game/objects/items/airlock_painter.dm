/obj/item/airlock_painter
	name = "airlock painter"
	desc = "An advanced autopainter preprogrammed with several paintjobs for airlocks. Click on it to change the paintjob and use it on airlocks to paint them."
	icon = 'icons/obj/objects.dmi'
	icon_state = "paint sprayer"
	item_state = "paint sprayer"

	w_class = WEIGHT_CLASS_SMALL

	custom_materials = list(/datum/material/iron=50, /datum/material/glass=50)

	flags_1 = CONDUCT_1
	item_flags = NOBLUDGEON
	slot_flags = ITEM_SLOT_BELT
	usesound = 'sound/effects/spray2.ogg'

	var/datum/airlock_paint/paint_style

	var/obj/item/toner/ink = null

/obj/item/airlock_painter/Initialize()
	. = ..()
	paint_style = new /datum/airlock_paint()
	ink = new /obj/item/toner(src)

//This proc doesn't just check if the painter can be used, but also uses it.
//Only call this if you are certain that the painter will be used right after this check!
/obj/item/airlock_painter/proc/use_paint(mob/user)
	if(can_use(user))
		ink.charges--
		playsound(src.loc, 'sound/effects/spray2.ogg', 50, TRUE)
		return 1
	else
		return 0

//This proc only checks if the painter can be used.
//Call this if you don't want the painter to be used right after this check, for example
//because you're expecting user input.
/obj/item/airlock_painter/proc/can_use(mob/user)
	if(!ink)
		to_chat(user, "<span class='warning'>There is no toner cartridge installed in [src]!</span>")
		return 0
	else if(ink.charges < 1)
		to_chat(user, "<span class='warning'>[src] is out of ink!</span>")
		return 0
	else
		return 1

/obj/item/airlock_painter/suicide_act(mob/user)
	var/obj/item/organ/lungs/L = user.getorganslot(ORGAN_SLOT_LUNGS)

	if(can_use(user) && L)
		user.visible_message("<span class='suicide'>[user] is inhaling toner from [src]! It looks like [user.p_theyre()] trying to commit suicide!</span>")
		use(user)

		// Once you've inhaled the toner, you throw up your lungs
		// and then die.

		// Find out if there is an open turf in front of us,
		// and if not, pick the turf we are standing on.
		var/turf/T = get_step(get_turf(src), user.dir)
		if(!isopenturf(T))
			T = get_turf(src)

		// they managed to lose their lungs between then and
		// now. Good job.
		if(!L)
			return OXYLOSS

		L.Remove(user)

		// make some colorful reagent, and apply it to the lungs
		L.create_reagents(10)
		L.reagents.add_reagent(/datum/reagent/colorful_reagent, 10)
		L.reagents.reaction(L, TOUCH, 1)

		// TODO maybe add some colorful vomit?

		user.visible_message("<span class='suicide'>[user] vomits out [user.p_their()] [L]!</span>")
		playsound(user.loc, 'sound/effects/splat.ogg', 50, TRUE)

		L.forceMove(T)

		return (TOXLOSS|OXYLOSS)
	else if(can_use(user) && !L)
		user.visible_message("<span class='suicide'>[user] is spraying toner on [user.p_them()]self from [src]! It looks like [user.p_theyre()] trying to commit suicide.</span>")
		user.reagents.add_reagent(/datum/reagent/colorful_reagent, 1)
		user.reagents.reaction(user, TOUCH, 1)
		return TOXLOSS

	else
		user.visible_message("<span class='suicide'>[user] is trying to inhale toner from [src]! It might be a suicide attempt if [src] had any toner.</span>")
		return SHAME


/obj/item/airlock_painter/examine(mob/user)
	. = ..()
	if(!ink)
		. += "<span class='notice'>It doesn't have a toner cartridge installed.</span>"
		return
	var/ink_level = "high"
	if(ink.charges < 1)
		ink_level = "empty"
	else if((ink.charges/ink.max_charges) <= 0.25) //25%
		ink_level = "low"
	else if((ink.charges/ink.max_charges) > 1) //Over 100% (admin var edit)
		ink_level = "dangerously high"
	. += "<span class='notice'>Its ink levels look [ink_level].</span>"


/obj/item/airlock_painter/attackby(obj/item/W, mob/user, params)
	if(istype(W, /obj/item/toner))
		if(ink)
			to_chat(user, "<span class='warning'>[src] already contains \a [ink]!</span>")
			return
		if(!user.transferItemToLoc(W, src))
			return
		to_chat(user, "<span class='notice'>You install [W] into [src].</span>")
		ink = W
		playsound(src.loc, 'sound/machines/click.ogg', 50, TRUE)
	else
		return ..()

/obj/item/airlock_painter/attack_self(mob/user)
	var/action
	if(ink)
		if(ink.charges < 1)
			action = "Remove Toner"
		else
			action = alert("What do you want to do?","Airlock Painter", "Change Style", "Remove Toner")
	else
		action = "Change Style"

	if(action == "Change Style")
		var/list/optionlist = list("Custom","Standard","None","Atmospherics","Command","Engineering","External Maintenance","External","Freezer","Maintenance","Medical","Mining","Research","Science","Security","Virology")
		var/paintjob = input(user, "Please select a paintjob for this airlock.") in optionlist
		switch(paintjob)
			if("None")
				paint_style = new /datum/airlock_paint()
			if("Standard")
				paint_style = new /datum/airlock_paint/public()
			if("Engineering")
				paint_style = new /datum/airlock_paint/engineering()
			if("Atmospherics")
				paint_style = new /datum/airlock_paint/atmospherics()
			if("Security")
				paint_style = new /datum/airlock_paint/security()
			if("Command")
				paint_style = new /datum/airlock_paint/command()
			if("Medical")
				paint_style = new /datum/airlock_paint/medical()
			if("Research")
				paint_style = new /datum/airlock_paint/science()
			if("Freezer")
				paint_style = new /datum/airlock_paint/freezer()
			if("Science")
				paint_style = new /datum/airlock_paint/science()
			if("Virology")
				paint_style = new /datum/airlock_paint/virology()
			if("Mining")
				paint_style = new /datum/airlock_paint/supply()
			if("Maintenance")
				paint_style = new /datum/airlock_paint/maintenance()
			if("External")
				paint_style = new /datum/airlock_paint/external()
			if("External Maintenance")
				paint_style = new /datum/airlock_paint/external_maint()
			if("Custom")
				paint_style = new /datum/airlock_paint()
				if(alert("Do you want to paint the frame?","Frame Paint", "Yes", "No") == "Yes")
					var/full_color = input("Choose a color for the airlock") as color
					if(full_color)
						paint_style.frame_paint["full"] = full_color
						paint_style.fill_paint["full"] = full_color
				if(alert("Do you want to paint a stripe?", "Fill Paint", "Yes", "No") == "Yes")
					var/stripe_color = input("Choose a color for the stripe") as color
					if(stripe_color)
						paint_style.frame_paint["stripe"] = stripe_color
						paint_style.fill_paint["stripe"] = stripe_color
	if(action == "Remove Toner")
		playsound(src.loc, 'sound/machines/click.ogg', 50, TRUE)
		ink.forceMove(user.drop_location())
		user.put_in_hands(ink)
		to_chat(user, "<span class='notice'>You remove [ink] from [src].</span>")
		ink = null
