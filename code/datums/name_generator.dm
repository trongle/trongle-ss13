/datum/global_name_gen/
	var/datum/name_gen/generic
	var/datum/name_gen/human
	var/datum/name_gen/lizard
	var/datum/name_gen/moth
	var/datum/name_gen/wizard

/datum/global_name_gen/New()
	var/datum/list_gen/weighted/generic_male = new(world.file2assoc("strings/names/generic/first_male.txt"))
	var/datum/list_gen/weighted/generic_female = new(world.file2assoc("strings/names/generic/first_female.txt"))
	var/datum/list_gen/weighted/generic_last = new(world.file2assoc("strings/names/generic/last.txt"))
	src.generic = new(generic_male, generic_female, generic_last)
	src.human = src.generic // humans are so boring

	var/datum/list_gen/weighted/lizard_male = new(world.file2assoc("strings/names/lizard/first_male.txt"))
	var/datum/list_gen/weighted/lizard_female = new(world.file2assoc("strings/names/lizard/first_female.txt"))
	var/datum/list_gen/weighted/lizard_last = new(world.file2assoc("strings/names/lizard/last.txt"))
	src.lizard = new(lizard_male, lizard_female, lizard_last)

	var/datum/list_gen/weighted/moth_male = new(world.file2assoc("strings/names/moth/first_male.txt"))
	var/datum/list_gen/weighted/moth_female = new(world.file2assoc("strings/names/moth/first_female.txt"))
	var/datum/list_gen/weighted/moth_last = new(world.file2assoc("strings/names/moth/last.txt"))
	src.moth = new(moth_male, moth_female, moth_last)

	var/datum/list_gen/weighted/wizard_male = new(world.file2assoc("strings/names/wizard/first_male.txt"))
	var/datum/list_gen/weighted/wizard_female = new(world.file2assoc("strings/names/wizard/first_female.txt"))
	var/datum/list_gen/wizard_last = new(world.file2list("strings/names/wizard/last.txt"))
	src.wizard = new(wizard_male, wizard_female, wizard_last)

// Name generator that simply picks random names from a list
/datum/name_gen
	var/datum/list_gen/first_m
	var/datum/list_gen/first_f
	var/datum/list_gen/last

/datum/name_gen/New(var/datum/list_gen/first_m, var/datum/list_gen/first_f, var/datum/list_gen/last)
	src.first_m = first_m
	src.first_f = first_f
	src.last = last

/datum/name_gen/proc/get_first(var/gender)
	if(gender == MALE)
		return src.first_m.select()
	if(gender == FEMALE)
		return src.first_f.select()

	var/datum/list_gen/picked = pick(src.first_m, src.first_f) 
	return picked.select()

/datum/name_gen/proc/get_last()
	return pick(src.last.select())

/datum/name_gen/proc/generate(var/gender)
	return "[src.get_first(gender)] [src.get_last()]"

// this simply will select a totally random entry from a list
/datum/list_gen
	var/list/entries

/datum/list_gen/New(var/list/data)
	src.entries = data

/datum/list_gen/proc/select()
	return pick(entries)

// this will select entries with respect to their weights
/datum/list_gen/weighted/New(var/list/data)
	var/sum = 0
	src.entries = new()

	// creates a distributed range of values 
	for(var/entry in data)
		sum += text2num(data[entry])
		src.entries[entry] = sum

/datum/list_gen/weighted/select()
	// rand() might break if the end range is very large
	var/sel = rand(0, src.entries[src.entries[src.entries.len]])

	for(var/entry in src.entries)
		if(sel <= src.entries[entry])
			return entry
	
	return pick(src.entries)
